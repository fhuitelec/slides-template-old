FROM fhuitelec/jekyll:3.4-alpine

MAINTAINER Fabien Huitelec < fabien at huitelec dot fr >

# Install dependencies
RUN apk update && apk add make

# Set initial environment
ADD . /jekyll
WORKDIR /jekyll

# Build
RUN jekyll build -s /jekyll -d /jekyll/_site
RUN rm -rf ./_site/2017

EXPOSE 4000
ENTRYPOINT jekyll serve \
	-H 0.0.0.0 \
	-d /jekyll/_site \
	-s /jekyll \
	--no-watch \
	--skip-initial-build