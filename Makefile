#!/usr/bin/make -f

###### INCLUDES ######
include .utils.make
######################

####################### EXPORTS ################################
export ENVIRONMENT = 'DEVELOPMENT'
export BRANCH_NAME = $(shell git rev-parse --abbrev-ref HEAD)
################################################################

.SILENT:

add-permalink: ## COMMAND-PROJECT Initialize slug (permalink) from the branch name (http://slides.huitelec.fr/branch-name)
	mkdir $(BRANCH_NAME)
	sed -i -e "s/{{permalink}}/${BRANCH_NAME}/g" _config.yml
	mv resources $(BRANCH_NAME)/resources
	rm _config.yml-e 

init-auth: ## COMMAND-PROJECT Initialize slides basic authentication. Use if you want to enable authentication
	read -p "Username: " username;\
	printf "AUTHENTICATED=true\nHTPASSWD=" >> .vars; \
	printf "%q" `htpasswd -n $$username|tr -d '\n'` >> .vars
	printf "\n\n" >> .vars

full-up: ## COMMAND-DEVELOPMENT Launch full jekyll stack to test authentication
full-up: .check-development
	BRANCH_NAME=$(shell git rev-parse --abbrev-ref HEAD) && \
	HTPASSWD='' AUTHENTICATED='' source .vars && \
	docker-compose -p slides -f ./infrastructure/development/docker-compose.yml up -d --build

up: ## COMMAND-DEVELOPMENT Launch jekyll server locally with file watch enabled
up: .check-development
	jekyll serve --watch

help: ## COMMAND Print this help
	make .notice message="Slides template Makefile"
	make .print message=""
	make .notice message="USAGE"
	make .print message="\tmake [development|production] COMMAND"
	make .print message="\tmake add-permalink"
	make .print message="\tmake init-auth"
	make .print message="\tmake development up"
	make .print message=""
	make .notice message="ENVIRONMENT"
	make .print-detailed-help type=ENVIRONMENT
	make .print message=""
	make .notice message="COMMAND"
	make .print-detailed-help type=COMMAND-PROJECT
	make .print message=""
	make .notice message="COMMAND DEVELOPMENT"
	make .print-detailed-help type=COMMAND-DEVELOPMENT

production: ## ENVIRONMENT Set environment to production
	$(eval export ENVIRONMENT = 'PRODUCTION')

development: ## ENVIRONMENT Set environment to development
    $(eval export ENVIRONMENT = 'DEVELOPMENT')