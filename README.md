# Slides templates

## Description

This repository allows you, dear Fabien, to easily deploy slides to expose them to a set of persons or to the public.

Let's say you create a branch `you-rock` from master, change a few information, run some `make` and create your slides. You commit everything and push to this repo and **BOOM**, a minute later, your slides are on `https://slides.huitelec.fr/your-rock`!

## Usage

### Initialization

- Create a branch from `master`, name it to the slug you want to access your slides from. e.g. `you-rock` will make these slides accessible from https://slides.huitelec.fr/you-rock
- Add your slides information (title, description, etc.) in `_config.yml`
- Run `make add-permalink`

### Authentication

If you need to add authentication, run `make init-auth`

### Development

- Run `make up` and iterate on your presentation
- Run `make full-up` to test authentication and the final slides

### Deployment

- Git add, commit and push
- Monitor the pipeline in [here](https://gitlab.com/fhuitelec/slides-template/pipelines). The slides will be deployed within 1-2 minutes
- Enjoy your presentation on https://slides.huitelec.fr

## Warning

Do **NOT EVER** push/merge on master, except to add enhancement!

## How it works

### Branch system

When you run a `run add-permalink`:
- a placeholder in `_config.yml` is replaced with the branch name to notify jekyll to build the slides in a specific folder of `_site`
- the `resources` folder is placed in a folder named with the branch name so when jekyll will builds the `_site` folder, it will place the resources in the right folder

### Authentication

The `.vars` file is already initialized with no authentication and an empty ht_password. 

When you run a `make init-auth`, the generated password will be appened to `.vars` and authentication will be enabled.

On development and production deployment, the `.vars` is sourced and used by docker-compose to create the nginx-auth proxy.